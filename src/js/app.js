"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Dropdown from "./components/dropdown.js";
import Spoilers from "./components/spoilers.js";
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import Rating from './components/rating.js'; // Rating plugin
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Notify from 'simple-notify'
import {WebpMachine} from "webp-hero"

const body = document.querySelector('body')

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Сворачиваемые блоки
collapse();

// Dropdown
Dropdown();

// Rating
Rating();

// Sliders
import "./components/sliders.js";

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Sticky header
document.addEventListener('scroll', (event) => {
    const header = document.querySelector('[data-header]')
    const sticky = document.querySelector('.header-sticky')

    if (sticky.getBoundingClientRect().top < 0) {
        header.classList.add("header--fixed");
    } else {
        header.classList.remove("header--fixed");
    }
})

// Speaker

document.addEventListener('click', (event) => {
    if(event.target.closest('[data-speaker-toggle]')) {
        event.preventDefault()
        event.target.closest('[data-speaker]').classList.toggle('speaker-item--open')
    }
})


// Sorter
if(document.querySelector('[data-sorter]')) {

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-sorter-button]')) {
            const sorter = event.target.closest('[data-sorter]')
            const sorterItem = event.target.closest('[data-sorter-button]')

            document.querySelectorAll('[data-sorter-button]').forEach(elem => {
                elem.classList.remove('active');
            });
            sorterItem.classList.add('active')
        }
    })
}


// Add favorite
document.addEventListener('click', (event) => {
    if(event.target.closest('[data-add-favorite]')) {
        event.target.closest('[data-add-favorite]').classList.toggle('added')
    }
})

const cardItems = document.querySelectorAll('.main-card')

cardItems.forEach(elem => {
    elem.onmouseover = function(event) {
        let location = elem.querySelector('.main-card__location')
        location.style.maxWidth = location.scrollWidth + 'px';
    };
    elem.onmouseout  = function(event) {
        let location = elem.querySelector('.main-card__location')
        location.style.maxWidth = null
    };
});

(() => {
    const commandGroup = document.querySelectorAll('.command-item');
    if(commandGroup.length > 0) {
        commandGroup.forEach(elem => {
            elem.onmouseover = function(event) {
                let location = elem.querySelector('.command-item__content')
                location.style.maxHeight = location.scrollWidth + 'px';
            };
            elem.onmouseout  = function(event) {
                let location = elem.querySelector('.command-item__content')
                location.style.maxHeight = null
            };
        });
    }

    document.addEventListener('click', (event) => {
        if(event.target.closest('[data-command-info]')) {
            event.preventDefault()
            const item = event.target.closest('.command-item')
            const itemImage = item.querySelector('img').src
            const itemTitle = item.querySelector('.command-item__title').innerHTML
            const itemText = item.querySelector('.command-item__text').innerHTML
            const itemLink = item.querySelector('.btn-blue').href

            const itemModal = document.querySelector('[data-command]')

            itemModal.querySelector('[data-command-image]').src = itemImage
            itemModal.querySelector('[data-command-title]').innerHTML = itemTitle
            itemModal.querySelector('[data-command-text]').innerHTML = itemText
            itemModal.querySelector('[data-command-link]').href = itemLink

            const fancybox = new Fancybox([
                {
                    src: "#command-info",
                    type: "inline",
                },
            ]);
        }
    })
})();

// Filter

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-filter-toggle]')) {
        body.classList.toggle('fixed-height')
        event.target.closest('[data-filter]').classList.toggle('-open-')
    }
})

