/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров

    if(document.querySelector('[data-speakers]')) {
        new Swiper('[data-speakers]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 10,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-speakers-next]',
                prevEl: '[data-speakers-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if(document.querySelector('[data-events]')) {
        new Swiper('[data-events]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 15,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-events-next]',
                prevEl: '[data-events-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    // Sorter
    if (document.querySelector('[data-sorter]')) {
        let sorterQuerySize  = 1280;
        let sorterSlider = null;

        function sorterSliderInit() {
            if(!sorterSlider) {
                sorterSlider = new Swiper('[data-sorter]', {
                    init: true,
                    loop: false,
                    freeMode: true,
                    spaceBetween: 10,
                    slidesPerView: 'auto',
                });
            }
        }

        function sorterSliderDestroy() {
            if(sorterSlider) {
                tagsSlider.destroy();
                tagsSlider = null;
            }
        }

        if (document.documentElement.clientWidth < sorterQuerySize) {
            sorterSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < sorterQuerySize) {
                sorterSliderInit()
            }
            else {
                sorterSliderDestroy()
            }
        });
    }

    if(document.querySelector('[data-archive-media]')) {
        new Swiper('[data-archive-media]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            observer: true,
            observeParents: true,
            spaceBetween: 10,
            slidesPerView: 1,
            initialSlide: 1,
            speed: 6000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false,
            },
        });
    }

    // Card
    if (document.querySelector('[data-cards]')) {
        let cardsQuerySize  = 1280;
        let cardsSlider = null;

        function cardsSliderInit() {
            if(!cardsSlider) {
                cardsSlider = new Swiper('[data-cards]', {
                    init: true,
                    loop: false,
                    freeMode: true,
                    spaceBetween: 12,
                    slidesPerView: 'auto',
                });
            }
        }

        function cardsSliderDestroy() {
            if(cardsSlider) {
                cardsSlider.destroy();
                cardsSlider = null;
            }
        }

        if (document.documentElement.clientWidth < cardsQuerySize) {
            cardsSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < cardsQuerySize) {
                cardsSliderInit()
            }
            else {
                cardsSliderDestroy()
            }
        });
    }

    // Trailer
    if (document.querySelector('[data-trailer]')) {

        new Swiper('[data-trailer]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            observer: true,
            observeParents: true,
            spaceBetween: 14,
            slidesPerView: 'auto',
            speed: 6000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false,
            },
        });

        new Swiper('[data-trailer-reverse]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            observer: true,
            observeParents: true,
            spaceBetween: 14,
            slidesPerView: 'auto',
            speed: 6000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false,
                reverseDirection: true
            },
        });
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
